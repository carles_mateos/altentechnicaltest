import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class Resume extends Component{

    constructor(props){
        super(props);
        this.state = {name:' ',age:' '};
        this.send = this.send.bind(this);
    }

    componentDidMount(){
        this.state.name = this.props.location.state.name;
        this.state.age = this.props.location.state.age;
        
    }
    send(){
        //console.log(this.state);
        //Dejo comentado este log por si se quiere ver que los datos realmente llegan hasta aquí.
        if(this.state.name == ""||this.state.age ==""){

            alert("Hey! You have to tell me your name and age, please don't be bad ;)");

        }else{
            alert("Name: " + this.state.name + " and Age: " + this.state.age + " sent to REST api. Congratulations :)");
            fetch('https://mywebsite.com/endpoint/', {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    name: this.state.name,
                    age: this.state.age
                })
            })
        }
    }
    render(){

        const nlists= ['This is a list item','Another list item','Yup, another list item','Another list item','This is a list item','Yup, another list item','This is a list item','Another list item','This is a list item','Yup, another list item']
        
        return(

            <div id='main'>
                
                <div id='title'>Resume</div>
                
                <div id='list_item'>
                {nlists.map((value,index)=>{

                    return <li id='list_item'>{value}</li>
                })}
                </div>
                
                <br/>
                
                <div>
                    
                    <button class='button' onClick={this.send}>Send</button>
                    
                </div>

                    
            </div>
            
        );
    }
}

export default Resume;