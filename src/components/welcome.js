import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class Welcome extends Component{

    constructor(props){
        super(props);
        this.state = {name:''};
        this.handleChange = this.handleChange.bind(this);
    }
    
    handleChange(event){
        this.setState({name: event.target.value})
    }

    render(){

        return(

            <div id='main'>

                <div id='title'>
                    <label >Welcome</label>
                </div>

                <div id='input'>
                    <label >What's your name?</label>
                    <br/>
                    <input type='text' onChange={this.handleChange} maxLength="10" ></input>
                    <hr/>
                </div>

                <div>
                    <Link to={{
                        pathname:'/secondPage',
                        state:{
                            name:this.state.name
                            
                        }
                    }
                        }>
                        <button class='button'>Next</button>
                    </Link>
                </div>   

                    
            </div>
            
        );
    }
}

export default Welcome;