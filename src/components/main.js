import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import Welcome from './welcome';
import SecondPage from './secondPage';
import Resume from './resume';

class Main extends Component{

    
  
    render(){
        return(
            <div id='main'>
                <Router>
                <Switch>
                <Route exact path='/' component={Welcome}/>
                <Route path='/secondPage' component={SecondPage}/>
                <Route path='/resume' component={Resume}/>
                </Switch>
 
                </Router>
                
            </div>
            
        );
    }
}

export default Main;