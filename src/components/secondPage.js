import React, {Component} from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

class SecondPage extends Component{

    constructor(props){
        super(props);
        this.state = {age:''};
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount(){
        this.state.name = this.props.location.state.name;
        //console.log(this.state);
    }
    

    handleChange(event){
        this.setState({age: event.target.value})
    }
    render(){

        return(
            <div>
            
                <div id='input2'>
                    <label >How old are you?</label>
                    <br/>
                    <input type='number' onChange={this.handleChange} max="99"></input>
                    <hr/>
                </div>

                <div>

                    <Link to={'/'}>
                                <button class='button2'>Back</button>
                    </Link>
                    
                    <Link to={ 
                            {
                                pathname:'/resume',
                                state:{
                                    name:this.state.name,
                                    age:this.state.age
                                }
                            }
                        }>
                        <button class='button2'>Next</button>
                    </Link>
                </div>
            
            
            </div>
        )
    }
}

export default SecondPage;